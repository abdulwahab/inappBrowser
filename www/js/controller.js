angular.module('controller', ['ionic'])

.controller('browserCtrl', function($scope, $state, $cordovaInAppBrowser ,$rootScope) {

    var options = {
        location: 'yes',
        clearcache: 'yes',
        toolbar: 'no'
    };

    $scope.app_Browser = function() {

        document.addEventListener("deviceready", function() {
            $cordovaInAppBrowser.open('http://ngcordova.com', '_blank', options)
                .then(function(event) {
                    // success
                    console.log("success");
                })
                .catch(function(event) {
                    // error
                });


            // $cordovaInAppBrowser.close();

        }, false);

        $rootScope.$on('$cordovaInAppBrowser:loadstart', function(e, event) {

        });

        $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event) {
            // insert CSS via code / file
            $cordovaInAppBrowser.insertCSS({
                code: 'body {background-color:blue;}'
            });

            // insert Javascript via code / file
            $cordovaInAppBrowser.executeScript({
                file: 'script.js'
            });
        });

        $rootScope.$on('$cordovaInAppBrowser:loaderror', function(e, event) {

        });

        $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event) {

        });
    }


})
